# customer-proximity

Reads a list of customers from a file and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).

## Usage
java -jar target/customer-proximity-0.1.0-SNAPSHOT-standalone.jar  [options] absolute-path-to-customer-file

###Options:
| short         | long option                   | Default value         | Description                |
| ------------- |:-----------------------------:| ---------------------:| --------------------------:|
| -r RADIUS     | --radius RADIUS               |   100                 | Radius in kilometers       |
| -o "LAT LON"  | --intercom-location "LAT LON" |   "53.3393 -6.2576841"| Lat Lon of intercom office |
| -h            |                               |                       | Show options               |

## Example: calling with defaults
java -jar target/customer-proximity-0.1.0-SNAPSHOT-standalone.jar /home/jcantwell/workspace/customer-proximity/32-customers-2-invalid.json
## Example calling with options
java -jar target/customer-proximity-0.1.0-SNAPSHOT-standalone.jar -r 888 -o "53.3393 -6.2576841"  /home/jcantwell/workspace/customer-proximity/32-customers-2-invalid.json