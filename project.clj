(defproject customer-proximity "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha16"]
                 [org.clojure/spec.alpha "0.1.94"]
                 [org.clojure/core.specs.alpha "0.1.10"]
                 [org.clojure/tools.cli "0.3.5"]
                 [cheshire "5.7.1"]]
  :profiles {:test {:resource-paths ["test/resources"]}}
  :aot  [customer-proximity.core]
  :main customer-proximity.core)
