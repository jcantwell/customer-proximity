(ns customer-proximity.core
  (:require [clojure.tools.cli :as cli]
            [customer-proximity.customers-within-radius :refer [find-customers-within-radius]]
            [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [clojure.pprint :refer :all])
  (:gen-class))

; definition of cmdline options with parsing, default values and validation
(def cli-options
  (let [parse-gps-coord-str (fn [coord-str]
                              (let [[lat lon]
                                    (map #(Double/valueOf (str/trim %))
                                         (str/split (str/trim coord-str)  #" "))]
                                {:lat lat :lon lon}))

        parse-radius-str (fn [radius]
                           (Integer/parseInt (str/trim radius)))

        validate-lat-lon (fn [coord]
                           (let [{lat :lat lon :lon} coord]
                             (and
                               (and (> lat -90) (< lat 90))
                               (and (> lon -180) (< lon 180)))))]

  [["-r" "--radius RADIUS" "Radius in kilometers"
    :default 100
    :parse-fn parse-radius-str
    :validate [#(> % 0) "Must be an integer greater than 0"]]

   ["-o" "--intercom-location LAT LON" "Lat Lon of intercom office"
    :default {:lat 53.3393 :lon -6.2576841}
    :parse-fn parse-gps-coord-str
    :validate [validate-lat-lon "Must be a valid gps coordinate"]]

   ["-h" "--help"]]))


(defn usage [options-summary]
  (->> ["Program reads in customer records and returns those that fall within a specified radius."
        ""
        "Usage: customers-in-range [options] customer-file"
        ""
        "Options:"
        options-summary
        ""]
       (str/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing the command:\n\n"
       (str/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map with
  the customer file path,radius and intercom-location values"
  [args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (:help options) ; help => exit OK with usage summary
        {:exit-message (usage summary) :ok? true}
      errors ; errors => exit with description of errors
        {:exit-message (error-msg errors)}
      ;;validation of customer file argument
      (not= 1 (count arguments))
        {:exit-message "Missing customer file argument"}
      (not (.exists (clojure.java.io/file (first arguments)))) ;invalid customer file
        {:exit-message "Invalid customer file argument, file does not exist. Needs to be the absolute path of the file"}
      :else ; proceed with program
      {:customer-file (first arguments) :radius (:radius options) :intercom-location (:intercom-location options)})))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [customer-file radius intercom-location exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (let [result (find-customers-within-radius customer-file radius intercom-location)]
        (print-table [:customer-entry/user_id :customer-entry/name :customer-entry/distance-from-office] (:customers-within-radius result))
        (print-table [:total :malformed-entries :customers-found-in-radius] [(:result-summary result)])
        )
      )))
