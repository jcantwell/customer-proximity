(ns customer-proximity.customer-input-file
  (:require [cheshire.core :as json]
            [clojure.spec.alpha :as s]
            [clojure.string :as str]))


(defn- valid-latitude?
  "Predicate used in customer entry spec"
  [lat]
  (try
    (let [double-lat (Double/valueOf lat)]
      (and
        (> double-lat -90)
        (< double-lat 90)))
    (catch Exception e
      false)))

(defn- valid-longitude?
  "Predicate used in customer entry spec"
  [lon]
  (try
    (let [double-lon (Double/valueOf lon)]
      (and
        (> double-lon -180)
        (< double-lon 180)))
    (catch Exception e
      false)))

;specification for a valid customer entry
(s/def :customer-entry/name string?)
(s/def :customer-entry/user_id integer?)
(s/def :customer-entry/latitude valid-latitude?)
(s/def :customer-entry/longitude valid-longitude?)
(s/def ::valid-customer-entry
  (s/keys :req [:customer-entry/name
                :customer-entry/user_id
                :customer-entry/latitude
                :customer-entry/longitude]))


(defn- create-valid-entry [result valid-entry]
  (let [customer-entry
        (assoc valid-entry  ;add a coords property to the customer-entry object
          :customer-entry/coords {
                                  :lat (Double/valueOf (:customer-entry/latitude valid-entry))
                                  :lon (Double/valueOf (:customer-entry/longitude valid-entry))
                                  })]
    (as-> result $
          (update-in $ [:entries] #(conj % customer-entry))  ;add valid entry to entries sequence
          (update-in $ [:summary :total] inc))))             ;update summary total count



(defn- create-invalid-entry [result index error-msg]
  (as-> result $
        (update-in $ [:entries] #(conj % {:row-num index :details error-msg})) ;add invalid entry to entries sequence
        (update-in $ [:summary :total] inc)                                    ;update summary total count
        (update-in $ [:summary :malformed-entries] inc)))                      ;update summary malformed-entries count


(defn- parse-customer-entry
  "Converts a line from a customer file into either a vaild-entry object or an error object.
  Also updates the summary object with details on the result of the parsing attempt"
  [result index line]
  (try
    (let [json-customer (json/parse-string line (fn [k](keyword (str "customer-entry/" k))))]

      (if (s/valid? ::valid-customer-entry json-customer)
        (create-valid-entry result json-customer)                 ;create a valid entry parsed entry matches the spec
        (create-invalid-entry                                     ;otherwise create an invalid entry with details of the validation error
          result
          index
          (s/explain-str ::valid-customer-entry json-customer))))
    (catch Exception e
      (create-invalid-entry result index (.getMessage e)))))      ;create an invalid entry if there was an exception for whatever reason, like a JsonParseException


(defn read-in-customer-entries
  "Reads in the customers file line by line and returns a result object with a sequence of the processed entries and a summary
  that has counts of the total number of entries processed and how many entries were invalid"
  [customer-file]
  (with-open [rdr (clojure.java.io/reader customer-file)]
    (reduce-kv parse-customer-entry
               {
                :entries []
                :summary {
                          :total             0
                          :malformed-entries 0
                          }}
               (-> (slurp rdr)
                   (clojure.string/split-lines)))))








