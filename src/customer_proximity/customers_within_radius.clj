(ns customer-proximity.customers-within-radius
  (:require [cheshire.core :as json]
            [customer-proximity.customer-input-file :refer [read-in-customer-entries]]
            [clojure.spec.alpha :as s]))


(defn- distance-in-km-between-2-locations
  "Implementation of Haversine formula - https://en.wikipedia.org/wiki/Haversine_formula.
  Takes two sets of latitude/longitude pairs and returns the distance between them (in km)"
  [{lat1 :lat lon1 :lon} {lat2 :lat lon2 :lon}]
  (let [haversin (fn [angle]
                    (Math/pow
                      (Math/sin (/ angle 2))
                      2))

        earth-radius-km 6378.137
        lat1-rad (Math/toRadians lat1)
        lat2-rad (Math/toRadians lat2)
        d-lat (Math/toRadians (- lat2 lat1))
        d-lon (Math/toRadians (- lon2 lon1))

        a (+ (haversin d-lat)
             (* (Math/cos lat1-rad) (Math/cos lat2-rad) (haversin d-lon)))]

    (* earth-radius-km 2 (Math/asin (Math/sqrt a)))))

(defn- assoc-distance-from-office [intercom-office-location customer]
  (assoc customer :customer-entry/distance-from-office
                  (distance-in-km-between-2-locations intercom-office-location (:customer-entry/coords customer))))

(defn- customer-within-radius? [radius {distance :customer-entry/distance-from-office} ]
  (> radius distance))

(defn- valid-customer-entry? [entry]
  (s/valid? :customer-proximity.customer-input-file/valid-customer-entry entry))

(defn find-customers-within-radius
  "Read in a lazy sequence of valid and invalid entries from a customer file, filter out invalid entries and customers
  that are outside the radius. Return a result with sorted sequence of customers inside the radius and a summary of processed entries."
  [customer-file radius intercom-office-location]
  (let [parsed-result (read-in-customer-entries customer-file)
        customers-found
        (->> (:entries parsed-result)
             (filter valid-customer-entry?)                                       ;filter any malformed entries
             (map (partial assoc-distance-from-office intercom-office-location))  ;calculate distance from office
             (filter (partial customer-within-radius? radius))                    ;filter any customers outside threshold
             ((partial sort-by :customer-entry/user_id)))]                        ;sort the filtered sequence by user_id
    {
     :customers-within-radius customers-found
     :result-summary          (assoc (:summary parsed-result) :customers-found-in-radius (count customers-found))
     }
    ))



