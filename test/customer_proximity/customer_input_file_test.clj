(ns customer-proximity.customer-input-file-test
  (:require [clojure.test :refer :all]
            [clojure.string :as str]
            [customer-proximity.customer-input-file :refer :all]))


(deftest customer-input-file-test

  (testing "parse-customer-entry"
    (let [valid-entry {
                       :entries
                                [#:customer-entry{
                                                  :latitude  "52.986375"
                                                  :longitude "-6.043701"
                                                  :user_id   12
                                                  :name      "Iggle Piggle"
                                                  :coords    {:lat 52.986375, :lon -6.043701}}]
                       :summary {
                                 :total             1
                                 :malformed-entries 0
                                 }}
          init-result {
                       :entries []
                       :summary {
                                 :total             0
                                 :malformed-entries 0
                                 }
                       }]
      (testing "should parse well formed input line"
        (is (= valid-entry
               (#'customer-proximity.customer-input-file/parse-customer-entry
                 init-result
                 0
                 "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Iggle Piggle\", \"longitude\": \"-6.043701\"}"))))

      (testing "should allow valid entry with extra property"
        (is (= (assoc-in valid-entry [:entries 0 :customer-entry/extra-prop] "ignored extra prop value")
               (#'customer-proximity.customer-input-file/parse-customer-entry
                 init-result
                 0
                 "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Iggle Piggle\", \"longitude\": \"-6.043701\", \"extra-prop\": \"ignored extra prop value\"}"))))

      (testing "should skip entry with missing required property (user_id)"
        (let [error-result
              (#'customer-proximity.customer-input-file/parse-customer-entry
                init-result
                3
                "{\"latitude\": \"52.986375\", \"name\": \"Iggle Piggle\", \"longitude\": \"-6.043701\"}")]

          (is (= 1
                 (get-in error-result [:summary :malformed-entries])))

          (is (= true
                 (str/includes?
                   (get-in error-result [:entries 0 :details])
                   "fails spec: :customer-proximity.customer-input-file/valid-customer-entry predicate: (contains? % :customer-entry/user_id)")))

          (is (= 3
                 (get-in error-result [:entries 0 :row-num])))))

      (testing "should skip entry with invalid json"
        (let [error-result
              (#'customer-proximity.customer-input-file/parse-customer-entry
                init-result
                3
                "{\"latitude\" \"92.986375\", \"user_id\": 12, \"name\": \"Iggle Piggle\", \"longitude\": \"-6.043701\"}")]

          (is (= 1
                 (get-in error-result [:summary :malformed-entries])))

          (is (= true
                 (str/includes?
                   (get-in error-result [:entries 0 :details])
                   "(code 34)): was expecting a colon to separate field name and value")))))

      (testing "should skip entry with string user_id value"
        (let [error-result
              (#'customer-proximity.customer-input-file/parse-customer-entry
                init-result
                33
                "{\"latitude\": \"52.986375\", \"user_id\": \"12\", \"name\": \"Iggle Piggle\", \"longitude\": \"-6.043701\"}")]

          (is (= 1
                 (get-in error-result [:summary :malformed-entries])))

          (is (= true
                 (str/includes?
                   (get-in error-result [:entries 0 :details])
                   "fails spec: :customer-entry/user_id at: [:customer-entry/user_id] predicate: integer?")))))

      (testing "should skip entry with invalid latitude value"
        (let [error-result
              (#'customer-proximity.customer-input-file/parse-customer-entry
                init-result
                3
                "{\"latitude\": \"92.986375\", \"user_id\": 12, \"name\": \"Iggle Piggle\", \"longitude\": \"-6.043701\"}")]

          (is (= 1
                 (get-in error-result [:summary :malformed-entries])))

          (is (= true
                 (str/includes?
                   (get-in error-result [:entries 0 :details])
                   "fails spec: :customer-entry/latitude at: [:customer-entry/latitude] predicate: valid-latitude?")))))

      (testing "should skip entry with invalid longitude value"
        (let [error-result
              (#'customer-proximity.customer-input-file/parse-customer-entry
                init-result
                3
                "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Iggle Piggle\", \"longitude\": \"-196.043701\"}")]

          (is (= 1
                 (get-in error-result [:summary :malformed-entries])))

          (is (= true
                 (str/includes?
                   (get-in error-result [:entries 0 :details])
                   "fails spec: :customer-entry/longitude at: [:customer-entry/longitude] predicate: valid-longitude?")))))))



  (testing "read-in-customer-entries"
    (testing "should generate a result with entities and summary counts from a customer file"
      (let [result (read-in-customer-entries "test/resources/32-customers-2-invalid.json")]
        (is (= 32
               (get-in result [:summary :total])))

        (is (= 2
               (get-in result [:summary :malformed-entries])))))))




