(ns customer-proximity.customers-within-radius-test
  (:require [clojure.test :refer :all]
            [customer-proximity.customers-within-radius :refer :all]))


(deftest customers-within-radius

  (defn roughly? [val1 val2]
    (> 1
       (Math/abs (- val1 val2))))

  (testing "distance-in-km-between-2-locations"
    (is (roughly? 41.76
                  (#'customer-proximity.customers-within-radius/distance-in-km-between-2-locations
                    {:lat 52.986375 :lon -6.043701}
                    {:lat 53.3393 :lon -6.2576841})))

    (is (roughly? 0.549
                  (#'customer-proximity.customers-within-radius/distance-in-km-between-2-locations
                    {:lat 38.898556 :lon -77.037852}
                    {:lat 38.897147 :lon -77.043934}))))

  (testing "find-customers-within-radius"

    (testing "should filter invalid and out of range entries"
      (let [result
            (find-customers-within-radius "test/resources/10-customers-3-within-radius-5-invalid.json"
                                          100
                                          {:lat 53.3393 :lon -6.2576841})]

        (is (=
              1
              (get-in result [:result-summary :malformed-entries])))

        (is (=
              3
              (get-in result [:result-summary :customers-found-in-radius])))

        (is (=
              3
              (count (:customers-within-radius result))))))

    (testing "should sort results by user_id"
      (let [result
            (find-customers-within-radius "test/resources/10-customers-3-within-radius-5-invalid.json"
                                          100
                                          {:lat 53.3393 :lon -6.2576841})]
        (is (=
              [6 8 26]
              (map :customer-entry/user_id (:customers-within-radius result)))))))
  )




